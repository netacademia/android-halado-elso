# Android-Halado-Elso

## Link gyűjtemény

* [Linear layout](https://www.tutorialspoint.com/android/android_linear_layout.htm)
* [Relative layout](https://www.tutorialspoint.com/android/android_relative_layout.htm)
* [Dashboard](https://developer.android.com/about/dashboards/)
* [Support packages](https://developer.android.com/topic/libraries/support-library/packages)
* [Launch filter](https://stackoverflow.com/questions/9695522/how-to-specify-which-activity-starts-on-app-launch)
* [Lorem ipsum](https://hu.lipsum.com/feed/html)
* [Barrier guideline különbség](https://stackoverflow.com/questions/47114672/what-is-difference-between-barrier-and-guideline-in-constraint-layout)
* [Lifecycle](https://i.stack.imgur.com/fRxIQ.png)

## További hasznos olvasnivaló

* [Developer site - constraint layout](https://developer.android.com/training/constraint-layout)
* [Developer site - layouts](https://developer.android.com/guide/topics/ui/declaring-layout)
* [16ms limit bővebben](https://medium.com/google-developers/exceed-the-android-speed-limit-b73a0692abc1)


